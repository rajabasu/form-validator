const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');

//Show input error message
function showError(input, message) {
  const formGroup = input.parentElement;
  formGroup.className = 'form-group error';
  const small = formGroup.querySelector('small');
  small.innerText = message;
}

//Show success outline
function showSuccess(input) {
  const formGroup = input.parentElement;
  formGroup.className = 'form-group success';
}

//Check required fields
function checkRequired(inputArr) {
  inputArr.forEach((input) => {
    console.log(input.value);
    if (input.value.trim() === '') {
      showError(input, `${getFieldName(input)} is required`);
    } else {
      showSuccess(input);
    }
  });
}

//Check input length for username and password
// function checkLength(input, min, max) {
//   if (input.value.length < min) {
//     showError(
//       input,
//       `${getFieldName(input)} must be atleast ${min} characters`
//     );
//   } else if (input.value.length > max) {
//     showError(
//       input,
//       `${getFieldName(input)} must be less than ${max} characters`
//     );
//   } else {
//     showSuccess(input);
//   }
// }

//Check Password
function checkPassword(input1, input2) {
  if (input1.value !== input2.value) {
    showError(input2, 'Passwords do not match. Re-type password.');
  }
}

//Get Field Name
function getFieldName(input) {
  return input.id.charAt(0).toUpperCase() + input.id.slice(1);
}

//Event Listener
form.addEventListener('submit', function (e) {
  e.preventDefault();

  checkRequired([username, email, password]);

  if (password2.value === '') {
    showError(password2, 'Re-type your password');
  } else {
    showSuccess(password2);
  }

  // checkLength(username, 3, 30);
  // checkLength(password, 6, 30);
  checkPassword(password, password2);
});
