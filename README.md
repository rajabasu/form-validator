# **Form Validation**

**This is my personal project, and I am creating a form validator using HTML, CSS, JavaScript, and Bootstrap.**

Here everything is validated from empty input field in username, email, password, confirm password to whether your passwords matches or not in the password and confirm password field

- Bootstrap CDN link - [Bootstarp](https://getbootstrap.com/docs/4.5/getting-started/introduction/)
- Google fonts - [Fonts](https://fonts.google.com/specimen/Open+Sans?query=open+sans&selection.family=Open+Sans:wght@300&sidebar.open)
